from setuptools import setup

def readme():
      with open('README.md') as f:
            return f.read()

setup(name='vrmanagerclient',
      version='0.1',
      long_description=readme(),
      description='Download Project Data from VRManager',
      url='https://bitbucket.org/mobivention/lokalizesync',
      author='Sören Busch',
      author_email='sbusch@mobivention.com',
      license='MIT',
      packages=['vrmanagerclient'],
      entry_points={
            'console_scripts': [
                  'vrmanagerclient=vrmanagerclient:cli'
            ]
      },
      zip_safe=False,
      install_requires=[
            'requests',
            'six',
            'cookiecutter'
      ])