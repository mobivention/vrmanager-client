VRManager Client
================

Installation
------------
If on Windows: Install Python (2.7+ or 3.4+ should both be fine) from https://www.python.org/downloads/. Open a command line. Run `python` and `pip`. If anything fails, make sure your python-path was added to PATH.
If you want to use project generation install the git CLI: https://git-scm.com/download/win

Please use the following command to install or upgrade the package:

	pip install -U https://bitbucket.org/mobivention/vrmanager-client/get/master.zip
    
If you get permission errors (i.e. you are on Mac OS and not in a virtualenv), please use this one:

	pip install -U --user https://bitbucket.org/mobivention/vrmanager-client/get/master.zip
	
Project Generation
------------------

To create a unity project for a given VRManager project run the following command

    vrmanagerclient create PROJECT_SLUG
    
If you want to install it to a different folder than the current one, use the `--target` flag:
    
    vrmanagerclient create PROJECT_SLUG --target PATH/TO/FOLDER

Configuration
-------------

For any existing unity project, add a file named `vrmanager.json` your Project root directory. It should look like this:

	{
      "baseUrl": "http://192.168.1.4:7000",
      "projectSlug": "blab2",
      "outputDirectory": "movies"
    }

(outputDirectory is optional and relative to to the project's 'Assets/StreamingAssets' folder)

Update
------

Run the command `vrmanagerclient update` from your project's root directory.