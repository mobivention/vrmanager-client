import requests
import os
import json
import errno
import click
from cookiecutter.main import cookiecutter

try:
    from urllib.parse import urljoin
    from configparser import ConfigParser
except ImportError:
    from urlparse import urljoin
    from ConfigParser import ConfigParser


def download_project(project_slug, output_folder, base_url="http://localhost:4000"):

    try:
        os.makedirs(output_folder)
    except Exception as e:
        print(e)

    metadata_req = requests.get(urljoin(base_url, "api/project/%s" % project_slug))
    if metadata_req.status_code != 200:
        if metadata_req.status_code == 404:
            print("This Project does not exist")
        else:
            print("Download Error: %s" % metadata_req.text)
        return
    metadata = metadata_req.json()
    print(metadata)

    with open(os.path.join(output_folder, "project.json"), "w") as f:
        json.dump(metadata, f)

    for movie in metadata['movies']:
        filename = movie['file']
        if filename:
            download_movie_file(filename, movie, output_folder)

            download_preview_image(movie, output_folder)

        for marker in movie['markers']:
            download_marker_icon(marker, output_folder)

    for support_file in metadata.get('support_files', []):
        try:
            os.makedirs(support_file['target_path'])
        except Exception as e:
            pass
        out_file = os.path.join(support_file['target_path'], support_file['file'])
        req = requests.get(support_file['download_url'], stream=True)
        print(out_file)

        with open(out_file, 'wb') as fd:
            for chunk in req.iter_content():
                fd.write(chunk)


def download_movie_file(filename, movie, output_folder):
    out_file = os.path.join(output_folder, filename)
    req = requests.get(movie['download_url'], stream=True)
    print(out_file)
    with open(out_file, 'wb') as fd:
        fd.write(req.content)


def download_preview_image(movie, output_folder):
    preview = movie['preview_image']
    if preview:
        out_preview = os.path.join(output_folder, preview)
        req = requests.get(movie['preview_image_url'], stream=True)

        print(out_preview)
        with open(out_preview, 'wb') as pfd:
            pfd.write(req.content)


def download_marker_icon(marker, output_folder):
    icon = marker['icon']
    if icon:
        out_icon = os.path.join(output_folder, icon)
        req = requests.get(marker['icon_url'], stream=True)

        print(out_icon)
        with open(out_icon, 'wb') as pfd:
            pfd.write(req.content)

@click.group()
def cli():
    pass


@cli.command()
@click.option('--folder', default='.', help='Root folder of given VRManager project')
def update(folder):
    """Download all necessary data for given VRManager project"""
    _update(folder)


def _update(folder):
    os.chdir(folder)
    try:
        with open(os.path.join(os.getcwd(), "vrmanager.json")) as config_file:
            config = json.load(config_file)
            base_url = config["baseUrl"]
            project_slug = config["projectSlug"]
            out_dir = os.path.join("Assets/StreamingAssets", config.get("outputDirectory", ""))
            download_project(project_slug, out_dir, base_url)
    except IOError as e:
        if e.errno == errno.ENOENT:
            print("No Config file present. Please create vrmanager.json")
        else:
            print(e)


@cli.command()
@click.option('--url', default='http://192.168.1.4:7000', help='VRManager Base URL')
@click.option('--target', default='.', help='Target folder name')
@click.argument('project_slug')
def create(project_slug, url, target):
    """Generate a Unity Project from a given VRManager project and download all necessary data"""
    print("=== Get Project Metadata ===")

    full_url = urljoin(url, '/api/project/{slug}'.format(slug=project_slug))
    r = requests.get(full_url)
    data = r.json()
    name = data['name']

    print(data)
    print("=== Create project from cookiecutter ===")

    cookiecutter(template='http://192.168.1.4:7001/VRCookieCutter.git/', no_input=True, extra_context={'repo_name': name}, output_dir=target)

    print("=== Project instantiated ===")

    vrmanager_json = {
        "baseUrl": url,
        "projectSlug": project_slug
    }
    os.chdir(os.path.join(target, name))
    with open('vrmanager.json', 'w') as f:
        json.dump(vrmanager_json, f)
    print("=== Download project Data ===")
    _update('.')

if __name__ == '__main__':
    cli()
